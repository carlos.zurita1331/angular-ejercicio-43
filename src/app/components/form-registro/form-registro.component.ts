import { Component, OnInit } from '@angular/core';
import { AbstractControl, AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidadoresService } from 'src/app/servicios/validadores.service';

@Component({
  selector: 'app-form-registro',
  templateUrl: './form-registro.component.html',
  styleUrls: ['./form-registro.component.css']
})
export class FormRegistroComponent implements OnInit {

    fRegistro!:FormGroup;
  constructor(private fb: FormBuilder,
              private validadores:ValidadoresService) {
    this.crearForm1();
   }

   get usuarioNoValido1(){
     return this.fRegistro.get('usuario')?.invalid && this.fRegistro.get('usuario')?.touched;
   }

   get pass1NoValido(){
     return this.fRegistro.get('pass1')?.invalid && this.fRegistro.get('pass1')?.touched;
   }
   get pass2NoValido(){
     const pass1 = this.fRegistro.get('pass1')?.value;
     const pass2 = this.fRegistro.get('pass2')?.value;

     return(pass1 == pass2)?false:true;

   }

  ngOnInit(): void {
    //localStorage.clear()
  }

  crearForm1():void{
    this.fRegistro=this.fb.group({
    usuario :['',[Validators.required,Validators.maxLength(15),Validators.pattern(/^[a-z0-9_-]{3,16}$/)]],
    pass1:['',Validators.required],
    pass2:['',[Validators.required]]
    
    }
    ,{ Validators:this.validadores.passwordsIguales('pass1','pass2')} as AbstractControlOptions
    );
  }

  guardar():void{
    console.log(this.fRegistro.value);
    console.log("el formulario esta corriendo");
  }

  limpiarForm():void{
    this.fRegistro.reset()
  }

  envioDatos():void{
    let datos = JSON.stringify(this.fRegistro.value)
    localStorage.setItem('datos',(datos)) 
    //localStorage.clear()
  }

}
function w(arg0: number, w: any, arg2: { 5: any; 29: any; }, $: any): any {
  throw new Error('Function not implemented.');
}

